# coding: utf-8


def funa(a, b, c):
    """ exactly 3 arguments (required)
    """
    print(a, b, c)

funa(1, 2, 'toto')


def funb(*args):
    """ from 0 to infinity arguments
    """
    print(args)

funb(1, 2)
funb(1, 2, 3, 4)
funb(list('qlkekejoZJLDSKNVLDKsnvLekfqsdck'))
funb(*list('qlkekejoZJLDSKNVLDKsnvLekfqsdck'))
funb()


def func(a, b=None):
    """ 1 required argument, one optional named argument
    """
    print(a, b)

func(1)
func(1, 2)
try:
    func(1, 2, 3)
except TypeError as e:
    print("Error: %s" % e)


def fund(a, *args):
    """ 1 required argument, infinity of optional anonymous arguments
        args is always a tuple
    """
    print(a, args)

fund(1)
fund(1, 2)
fund(*list('qlkekejoZJLDSKNVLDKsnvLekfqsdck'))
try:
    fund()
except TypeError as e:
    print("Error: %s" % e)


def fune(a, b=None, c=None):
    print(a, b, c)

fune(1)
fune(1, b='toto')
fune(1, c='toto')


def funf(a, b=None, *args):
    print(a, b, args)

funf(1, 2)
funf(1, 2, 3)
try:
    funf(1, 2, b=3)
except TypeError as e:
    print("Error: %s" % e)


def fung(a, b=None, **kwargs):
    """ 1 required argument, one optional named argument, infinity of optional named arguments
    """
    print(a, b, kwargs)

fung(1)
fung(1, b=2, c=3, d=4)


def funh(a, b=None, *args, **kwargs):
    """ 1 required argument, one optional named argument, infinity of optional anonymous arguments,
        infinity of optional named arguments
    """
    print(a, b, args, kwargs)

funh(1)
funh(1, 2, 3, 4)
funh(1, 2, c=3, d=4)
funh(1, 2, 3, 4, x='toto', y='titi')
