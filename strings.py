# coding: utf-8


if __name__ == '__main__':
    phrase = "If you already have files you can push them using command line instructions below"
    print(phrase)
    print("len(phrase)=%s (chars)" % len(phrase))
    print(phrase.split())
    print("len(phrase.split())=%s (words)" % len(phrase.split()))

    print("How many 'in' in phrase: %s" % phrase.count('in'))

    assert 'in' in phrase, "'in' not in phrase"

    print("Uppercase: %s" % phrase.upper())
    print("Titlecase: %s" % phrase.title())

    mot_clef = "mot_clef"
    print("%s, remplace '_' par '-': %s" % (mot_clef, mot_clef.replace('_', '-')))

    template = '+-'
    print(template * 20)

    title = 'value'
    value1 = 1.2345
    value2 = 3.142592
    print(title.center(20))
    print('+' * 20)
    print(str(value1).ljust(20))
    print('-' * 20)
    print(str(value2).rjust(20))
    print('-' * 20)
