# coding: utf-8


class A(object):
    pass


class B(A):
    pass


class C(object):
    def __init__(self, a, b=None):
        self.a = a
        self.b = b
    def __str__(self):
        return "%s(%s, %s)" % (self.__class__.__name__, self.a, self.b)


if __name__ == '__main__':
    a = A()
    b = B()
    print a
    print a.__class__
    print b.__class__
    print(dir(A))
    print(issubclass(B, A))
    c1 = C(1)
    c2 = C(2, 'toto')
    print(c1)
    print(c2)
