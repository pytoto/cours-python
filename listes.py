# coding: utf-8


if __name__ == '__main__':
    l = [1, 2, 3]
    print("l=%s, len(l)=%s" % (l, len(l)))
    print("l[0]={}, l[2]={}, l[-1]={}".format(l[0], l[2], l[-1]))
    print("l[0:-1]={}".format(l[0:-1]))
    print("l[::2]={}".format(l[::2]))
    print("l[::-1]={}".format(l[::-1]))

    assert 1 in l, '1 not in l'

    try:
        l[10]
    except IndexError as e:
        print("Exception {}".format(e))
    print("l[:10]={} (pas d'exception)".format(l[:10]))

    l.append(4)
    print("l=%s, len(l)=%s" % (l, len(l)))
    l.extend((5, 6))
    print("l=%s, len(l)=%s" % (l, len(l)))

    print(list('python'))

    m = range(1, 7)
    print("range(7)=%s, range(1, 7)=%s, range(7)==l: %s" % (range(7), m, m == l))
