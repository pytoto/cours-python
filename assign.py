# coding: utf-8

a = (1, 2, 3)
print(a)

a, b, c = (1, 2, 3)
print(a, b, c)

a, *b = (1, 2, 3)  # this raises in python2 at compile time
print(a, b)
