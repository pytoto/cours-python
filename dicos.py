# coding: utf-8


d1 = {
    'k1': 'val1',
    'k2': 'val2',
    'k4': 'val3',
}

key = ('k1', 'k2', 'k4')
val = ('val1', 'val2', 'val3')

d2 = dict(zip(key, val))

print(d1)
print(d2)
assert d1 == d2, 'pas égal'
assert 'k1' in d1, 'k1 is not a key of d1'

print(d1['k1'])
try:
    print(d1['toto'])
except KeyError as e:
    print("%s is not a key" % e)

d1.update({'h1': 'toto'}, p1='titi')

print(d1)

print(list(d1))
print(d1.keys())
print(d1.values())
print(d1.items())

for k in d1:
    print(k)

for k, v in d1.iteritems():
    print("key %s, value %s" % (k, v))

print(d1.iteritems())

assert d1.items() == list(d1.iteritems())
